module Numeric.LinearAlgebra.Helpers
    ( module Numeric.LinearAlgebra.Helpers
    , ODEMethod(..)
    , MinimizeMethod(..), MinimizeMethodD(..), UniMinimizeMethod(..)
    , UniRootMethod(..), UniRootMethodJ(..), RootMethod(..), RootMethodJ(..)
    )
  where

import Control.Monad (forM_, guard)
import Control.Monad.ST (runST)
import Data.Array.Repa as R
import Data.Array.Repa.Eval (Target, fromList)
import Data.Array.Repa.Repr.ForeignPtr
import Data.Array.Repa.Repr.Vector

import Data.List (foldl', sortOn, group)
import Data.Tuple.Extra (first)
import qualified Data.Map.Strict as Map

import qualified Data.Vector as Vec
import qualified Data.Vector.Generic as VecG
import qualified Data.Vector.Storable as VecS
import qualified Data.Vector.Storable.Mutable as MutS
import qualified Data.Vector.Unboxed as VecU
import Foreign.Storable (Storable)

import Numeric.LinearAlgebra.Repa (dot)
import Numeric.LinearAlgebra.Repa.Conversion

import qualified Numeric.GSL.ODE as O
import Numeric.GSL.ODE (ODEMethod(..))
import qualified Numeric.GSL.Minimization as M
import Numeric.GSL.Minimization (MinimizeMethod(..), MinimizeMethodD(..), UniMinimizeMethod(..))
import qualified Numeric.GSL.Root as R
import Numeric.GSL.Root (UniRootMethod(..), UniRootMethodJ(..), RootMethod(..), RootMethodJ(..))
import qualified Numeric.LinearAlgebra as H
import qualified Numeric.LinearAlgebra.Data as H

-- Convenience aliases and instances

type Vec a = Array F DIM1 a
type Mat a = Array F DIM2 a

instance (Show a, Element a) => Show (Mat a) where
    show = show . repa2hm

instance (Show a, Element a) => Show (Vec a) where
    -- hmatrix displays vectors as "fromList [1,2,3]", which might be confusing
    show v = show (length l) Prelude.++ " |> " Prelude.++ show l
        where l = toList v

-- Scale vector/matrix by scalar

-- pure repa version
scale :: (Num a, Shape sh, Source r a) => a -> Array r sh a -> Array D sh a
scale = R.map . (*)

-- hmatrix versions
hscale :: (HShape sh, H.Numeric a, H.Linear a (HType sh)) => a -> Array F sh a -> Array F sh a
hscale c = (toRepa . H.scale c) . fromRepa
hscaleS :: (HShape sh, H.Numeric a, H.Linear a (HType sh)) => a -> Array D sh a -> Array F sh a
hscaleS c = (toRepa . H.scale c) . fromRepaS

hscaleSIO :: (HShape sh, H.Numeric a, H.Linear a (HType sh)) => a -> Array D sh a -> IO (Array F sh a)
hscaleSIO c = fmap (toRepa . H.scale c) . fromRepaSIO

hscaleP :: (Monad m, HShape sh, H.Numeric a, H.Linear a (HType sh)) => a -> Array D sh a -> m (Array F sh a)
hscaleP c = fmap (toRepa . H.scale c) . fromRepaP

hscalePIO :: (HShape sh, H.Numeric a, H.Linear a (HType sh)) => a -> Array D sh a -> IO (Array F sh a)
hscalePIO c = fmap (toRepa . H.scale c) . fromRepaPIO

-- Transpose matrix

htrans :: (H.Numeric a, Container H.Vector a) => Mat a -> Mat a
htrans = toRepa . H.tr . fromRepa

htransS :: (H.Numeric a, Container H.Vector a) => Array D DIM2 a -> Mat a
htransS = toRepa . H.tr . fromRepaS

htransSIO :: (H.Numeric a, Container H.Vector a) => Array D DIM2 a -> IO (Mat a)
htransSIO = fmap (toRepa . H.tr) . fromRepaSIO

htransP :: (Monad m, H.Numeric a, Container H.Vector a) => Array D DIM2 a -> m (Mat a)
htransP = fmap (toRepa . H.tr) . fromRepaP

htransPIO :: (H.Numeric a, Container H.Vector a) => Array D DIM2 a -> IO (Mat a)
htransPIO = fmap (toRepa . H.tr) . fromRepaPIO

-- TODO: asColumn, ...

-- Matrices and Vectors

(><) :: Target F a => Int -> Int -> [a] -> Mat a
(m><n) xs = fromList (ix2 m n) $ take (m*n) xs

(|>) :: Target F a => Int -> [a] -> Vec a
n |> xs = fromList (ix1 n) $ take n xs

vec :: Target F a => [a] -> Vec a
vec xs = fromList (ix1 $ length xs) xs

mat :: Target F a => [[a]] -> Mat a
mat xs
    | maxcols /= mincols = error "Rows have unequal length."
    | otherwise = fromList (ix2 (length xs) maxcols) $ concat xs
    where
      (maxcols, mincols) = foldl' (\(!mx, !mn) !l -> (max mx l, min mn l)) (0, maxBound) $
                           Prelude.map length xs

subVector :: Int -> Int -> Vec Double -> Vec Double
subVector s l = computeS . extract (ix1 s) (ix1 l)

-- Norms
norm2 :: Vec Double -> Double
norm2 v = sqrt(v `dot` v)

asColumn :: (Num a, Container H.Vector a) => Vec a -> Mat a
asColumn = hm2repa . H.asColumn . repa2hv

asColumnS :: (Num a, Container H.Vector a) => Array D DIM1 a -> Mat a
asColumnS = hm2repa . H.asColumn . repa2hvS

asColumnSIO :: (Num a, Container H.Vector a) => Array D DIM1 a -> IO (Mat a)
asColumnSIO = fmap (hm2repa . H.asColumn) . repa2hvSIO

asColumnP :: (Monad m, Num a, Container H.Vector a) => Array D DIM1 a -> m (Mat a)
asColumnP = fmap (hm2repa . H.asColumn) . repa2hvP

asColumnPIO :: (Num a, Container H.Vector a) => Array D DIM1 a -> IO (Mat a)
asColumnPIO = fmap (hm2repa . H.asColumn) . repa2hvPIO

asRow :: (Num a, Container H.Vector a) => Vec a -> Mat a
asRow = hm2repa . H.asRow . repa2hv

asRowS :: (Num a, Container H.Vector a) => Array D DIM1 a -> Mat a
asRowS = hm2repa . H.asRow . repa2hvS

asRowSIO :: (Num a, Container H.Vector a) => Array D DIM1 a -> IO (Mat a)
asRowSIO = fmap (hm2repa . H.asRow) . repa2hvSIO

asRowP :: (Monad m, Num a, Container H.Vector a) => Array D DIM1 a -> m (Mat a)
asRowP = fmap (hm2repa . H.asRow) . repa2hvP

asRowPIO :: (Num a, Container H.Vector a) => Array D DIM1 a -> IO (Mat a)
asRowPIO = fmap (hm2repa . H.asRow) . repa2hvPIO

diag :: (Num a, Storable a, Source r a) => Array r DIM1 a -> Array D DIM2 a
diag x = fromFunction (ix2 n n) f
    where
      n = size $ extent x
      f (Z:.i :.j) | i == j     = x ! (Z:.i)
                   | otherwise  = 0

diagOf :: Source r a => Array r DIM2 a -> Array D DIM1 a
diagOf m = fromFunction (ix1 n) f
    where
      n = minimum . listOfShape . extent $ m
      f (Z:.i) = m ! (Z:.i:.i)

ident :: (Num a, Storable a) => Int -> Array D DIM2 a
ident = diag . (|> repeat 1)

zeros :: (Num a, Shape sh) => sh -> Array D sh a
zeros = flip fromFunction $ const 0

ones :: (Num a, Shape sh) => sh -> Array D sh a
ones = flip fromFunction $ const 1

band :: Num a => [a] -> Int -> Array D DIM2 a
band xs n = if even (length xs)
    then error "band: only odd vector lengths allowed!"
    else fromFunction (ix2 n n) f
  where
    m = length xs `div` 2
    f (Z :. i :. j) | i == j            = xs !! m
                    | i < j && j-i <= m = xs !! (m+j-i)
                    | j < i && i-j <= m = xs !! (m-i+j)
                    | otherwise         = 0

-- extracting dimensions
vlength :: (Source a b) => Array a DIM1 b -> Int
vlength = vd1 . extent
  where vd1 (Z :. x) = x

cols :: (Source a b) => Array a DIM2 b -> Int
cols = md2 . extent
  where md2 (Z :. _ :. x) = x :: Int

rows :: (Source a b) => Array a DIM2 b -> Int
rows = md1 . extent
  where md1 (Z :. x :. _) = x

-- extracting slices
unsafeColumn :: (Source r e, Target r2 e) => Int -> Array r DIM2 e -> Array r2 DIM1 e
unsafeColumn i mat = computeS $ slice mat (Any :. All :. i)

column :: (Source r e) => Int -> Array r DIM2 e -> Array D DIM1 e
column i mat
    | cols mat < i = error "column: Index out of bounds"
    | otherwise    = slice mat (Any :. All :. (i :: Int))

unsafeRow :: (Source r e) => Int -> Array r DIM2 e -> Array D DIM1 e
unsafeRow i mat = slice mat (Any :. (i :: Int) :. All)

row :: (Source r e) => Int -> Array r DIM2 e -> Array D DIM1 e
row i mat
    | rows mat < i = slice mat (Any :. (i :: Int) :. All)
    | otherwise    = slice mat (Any :. (i :: Int) :. All)

linspace :: (Fractional e) => Int -> (e, e) -> Array D DIM1 e
linspace 0 _     = fromFunction (ix1 0) (const 0)
linspace 1 (a,b) = fromFunction (ix1 1) (const (a+b/2))
linspace n (a,b) = fromFunction (ix1 n) f
  where f (Z :. x) = a + (b-a)*fromIntegral x / fromIntegral (n-1)

-- ODEs

-- TODO IO instances
odeSolve :: (Double -> [Double] -> [Double])
    -> [Double] -> Vec Double -> Mat Double
odeSolve xdot xi ts = hm2repa $ O.odeSolve xdot xi $ repa2hv ts

odeSolveS :: (Double -> [Double] -> [Double])
    -> [Double] -> Array D DIM1 Double -> Mat Double
odeSolveS xdot xi ts = hm2repa $ O.odeSolve xdot xi $ repa2hvS ts

odeSolveP :: (Monad m) => (Double -> [Double] -> [Double])
    -> [Double] -> Array D DIM1 Double -> m (Mat Double)
odeSolveP xdot xi ts = hm2repa <$> (O.odeSolve <$> pure xdot <*> pure xi <*> repa2hvP ts)

odeSolveV ::
    ODEMethod
    -> Double -> Double -> Double
    -> (Double -> Vec Double -> Vec Double)
    -> Vec Double -> Vec Double -> Mat Double
odeSolveV meth hi epsAbs epsRel xdot xi ts =
    hm2repa $ O.odeSolveV meth hi epsAbs epsRel xdot' (repa2hv xi) (repa2hv ts)
  where
    xdot' x t = repa2hv $ xdot x (hv2repa t)

odeSolveVS = error "TODO"
odeSolveVP = error "TODO"

-- Conversion to vector types

class (Source r e, VecG.Vector (VType r) e) => Vectorable r e where
    type VType r :: * -> *
    toVec :: Array r sh e -> VType r e
    fromVec :: Shape sh => sh -> VType r e -> Array r sh e

instance Vectorable V e where
    type VType V = Vec.Vector
    toVec = toVector
    fromVec = fromVector

instance VecU.Unbox e => Vectorable U e where
    type VType U = VecU.Vector
    toVec = toUnboxed
    fromVec = fromUnboxed

instance Storable e => Vectorable F e where
    type VType F = VecS.Vector
    toVec (AForeignPtr _ len ptr) = VecS.unsafeFromForeignPtr0 ptr len
    fromVec sh v = AForeignPtr sh len ptr
        where (ptr, len) = VecS.unsafeToForeignPtr0 v

-- Sparse matrices

data Sparse = CSR { shape :: !DIM2
                  , csrVals :: !(Array U DIM1 Double)
                  , csrCols :: !(Array U DIM1 Int)
                  , csrRowidxs :: !(Array U DIM1 Int)
                  }
              deriving Show

spp :: Source r Double => Sparse -> Array r DIM1 Double -> Array D DIM1 Double
s `spp` x
  | m /= size (extent x) = error "Vector has wrong size"
  | otherwise = fromFunction (Z:.n) $ \(Z:.(!k)) ->
                let !rowidx = unsafeLinearIndex (csrRowidxs s) k
                    !num = unsafeLinearIndex (csrRowidxs s) (k+1) - rowidx
                    !vals = fromFunction (Z:.num) $ \(Z:.(!j)) ->
                            unsafeLinearIndex (csrVals s) (rowidx + j)
                    !xs = fromFunction (Z:.num) $ \(Z:.(!j)) ->
                          unsafeLinearIndex x $ unsafeLinearIndex (csrCols s) (rowidx + j)
                in sumAllS $ vals *^ xs
  where Z:.(!n):.(!m) = shape s

-- TODO: check for bounds and duplicates

toSparse :: DIM2 -> [(DIM2, Double)] -> Sparse
toSparse sh xs = CSR { shape = sh
                     , csrVals = fromList (Z:.n) vals
                     , csrCols = fromList (Z:.n) cols
                     , csrRowidxs = fromList (Z:.nrows+1) rowidxs
                     }
    where n = length xs
          Z:.nrows:._ = sh
          (idxs, vals) = unzip $ sortOn (toIndex sh . fst) xs
          (rows, cols) = unzip $ Prelude.map (\(Z:.j:.k) -> (j, k)) idxs
          lengths = foldl' (\m g -> Map.insert (head g) (length g) m) Map.empty $ group rows
          rowidxs = Prelude.scanl (\acc row -> acc + Map.findWithDefault 0 row lengths) 0 [0..nrows-1]

fromSparse :: Sparse -> [(DIM2, Double)]
fromSparse s = do
  let rowidxs = toList $ csrRowidxs s
  (row, start, end) <- zip3 [0..] rowidxs (tail rowidxs)
  idx <- [start .. end-1]
  let col = unsafeLinearIndex (csrCols s) idx
  let val = unsafeLinearIndex (csrVals s) idx
  return (Z:.row:.col, val)

spTranspose :: Sparse -> Sparse
spTranspose s = toSparse (tr $ shape s) . Prelude.map (first tr) . fromSparse $ s
    where tr (Z:.j:.k) = Z:.k:.j

toDense :: Sparse -> Mat Double
toDense s = AForeignPtr sh len ptr
    where
      sh = shape s
      (ptr, len) = VecS.unsafeToForeignPtr0 $ runST $ do
                     v <- MutS.replicate (size sh) 0
                     forM_ (fromSparse s) $ \(idx, val) ->
                         MutS.unsafeWrite v (toIndex sh idx) val
                     VecS.freeze v

fromDense :: Mat Double -> Sparse
fromDense m = toSparse (extent m) $ do
                let Z:.nrows:.ncols = extent m
                row <- [0..nrows-1]
                col <- [0..ncols-1]
                let idx = Z:.row:.col
                let val = unsafeIndex m idx
                guard $ val /= 0
                return (idx, val)

-- Minimization
minimize :: MinimizeMethod -> Double -> Int -> [Double] -> ([Double] -> Double) -> [Double] -> ([Double], Mat Double)
minimize method eps maxit sz f xi = trans $ M.minimize method eps maxit sz f xi
  where trans (xs,mat) = (xs,toRepa mat)

minimizeV :: MinimizeMethod -> Double -> Int -> Vec Double -> (Vec Double -> Double) -> Vec Double -> (Vec Double, Mat Double)
minimizeV method eps maxit szv f xiv = trans $ M.minimizeV method eps maxit (fromRepa szv) (f . toRepa) (fromRepa xiv)
  where trans (v,m) = (toRepa v, toRepa m)

minimizeD :: MinimizeMethodD
    -> Double                 -- ^ desired precision of the solution (gradient test)
    -> Int                    -- ^ maximum number of iterations allowed
    -> Double                 -- ^ size of the first trial step
    -> Double                 -- ^ tol (precise meaning depends on method)
    -> ([Double] -> Double)   -- ^ function to minimize
    -> ([Double] -> [Double]) -- ^ gradient
    -> [Double]               -- ^ starting point
    -> ([Double], Mat Double) -- ^ solution vector and optimization path
minimizeD method eps maxit istep tol f df xi= trans $ M.minimizeD method eps maxit istep tol f df xi
  where trans (xs,mat) = (xs,toRepa mat)

minimizeVD :: MinimizeMethodD
    -> Double                 -- ^ desired precision of the solution (gradient test)
    -> Int                    -- ^ maximum number of iterations allowed
    -> Double                 -- ^ size of the first trial step
    -> Double                 -- ^ tol (precise meaning depends on method)
    -> (Vec Double -> Double)   -- ^ function to minimize
    -> (Vec Double -> Vec Double) -- ^ gradient
    -> Vec Double               -- ^ starting point
    -> (Vec Double, Mat Double) -- ^ solution vec and optimization path
minimizeVD method eps maxit istep tol f df xiv = trans $ M.minimizeVD method eps maxit istep tol (f . toRepa) (fromRepa . df . toRepa) (fromRepa xiv)
  where trans (v,m) = (toRepa v, toRepa m)

uniMinimize :: UniMinimizeMethod -> Double -> Int -> (Double -> Double) -> Double -> Double -> Double -> (Double, Mat Double)
uniMinimize method epsrel maxit fun xmin xl xu = trans $ M.uniMinimize method epsrel maxit fun xmin xl xu
  where trans (xs,mat) = (xs, toRepa mat)


-- Root finding
uniRoot :: UniRootMethod
        -> Double
        -> Int
        -> (Double -> Double)
        -> Double
        -> Double
        -> (Double, Mat Double)
uniRoot method epsrel maxit fun xl xu = trans $ R.uniRoot method epsrel maxit fun xl xu
  where trans (d, m) = (d, toRepa m)

uniRootJ :: UniRootMethodJ
        -> Double
        -> Int
        -> (Double -> Double)
        -> (Double -> Double)
        -> Double
        -> (Double, Mat Double)
uniRootJ method epsrel maxit fun dfun x = trans $ R.uniRootJ method epsrel maxit fun dfun x
  where trans (ds, m) = (ds, toRepa m)

root :: RootMethod
     -> Double                     -- ^ maximum residual
     -> Int                        -- ^ maximum number of iterations allowed
     -> ([Double] -> [Double])     -- ^ function to minimize
     -> [Double]                   -- ^ starting point
     -> ([Double], Mat Double)  -- ^ solution vector and optimization path
root method epsabs maxit fun xinit = trans $ R.root method epsabs maxit fun xinit
  where trans (ds, m) = (ds, toRepa m)

rootJ :: RootMethodJ
      -> Double                     -- ^ maximum residual
      -> Int                        -- ^ maximum number of iterations allowed
      -> ([Double] -> [Double])     -- ^ function to minimize
      -> ([Double] -> [[Double]])   -- ^ Jacobian
      -> [Double]                   -- ^ starting point
      -> ([Double], Mat Double)  -- ^ solution vector and optimization path
rootJ method epsabs maxit fun jac xinit = trans $ R.rootJ method epsabs maxit fun jac xinit
  where trans (ds, m) = (ds, toRepa m)
