{-# LANGUAGE UndecidableInstances #-}

module Data.Array.Repa.Helpers
    ( CGState(..), cg, runCG, runCGreg
    , takeUntil, process
    , saveArrayAsPNG, saveArrayAsBMP, loadArrayFromBMP
    )
    where

import Control.Monad (foldM)
import Control.Monad.Identity (runIdentity)
import qualified Data.Array.Repa as R
import Data.Array.Repa hiding ((++))
import Data.Array.Repa.Eval (Load, Target)
import Data.Array.Repa.Algorithms.Pixel (doubleLuminanceOfRGB8, rgb8OfGreyDouble)
import Data.Array.Repa.IO.BMP (readImageFromBMP, writeImageToBMP)
import Data.Vector.Unboxed (Unbox)
import qualified Graphics.Rendering.Plot as P
import Numeric.LinearAlgebra.Repa.Conversion (repa2hm)

----------------------------
-- Conjugate gradient solver
----------------------------

-- CG solves linear equations
--
--   A x = y
--
-- for positive definite square matrices A.

class Shape sh => Unboxable r sh e where
    unboxP :: Monad m => Array r sh e -> m (Array U sh e)

instance {-# OVERLAPPABLE #-} (Shape sh, Load r sh e, Unbox e) => Unboxable r sh e where
    unboxP = computeP

instance {-# OVERLAPPING #-} Shape sh => Unboxable U sh e where
    unboxP = return

-- `CGState` is the state of a CG iteration, i.e. everything that needs to be
-- passed to the next iteration.

data CGState sh = CGState { cgx :: !(Array U sh Double)
                          , cgp :: !(Array U sh Double)
                          , cgr :: !(Array U sh Double)
                          , cgr2 :: !Double
                          }

-- `cg` takes a function implementing a linear operator, a right hand side and
-- an initial guess and returns the (lazy, infinte) list of the CGStates of all
-- iterations. This way, the iterations can be decoupled from the stopping
-- criterion and the output of diagnostic information.
--
-- Computations use Repa's parallel computation in a straight-forward way and
-- need to be performed in a monad. We use the Identity monad, which does
-- essentially nothing except for providing the sequencing. All operations are
-- performed strictly using BangPatterns.

cg :: (Shape sh, Unboxable r sh Double, Source r Double) => (Array U sh Double -> Array r sh Double) -> Array U sh Double -> Array U sh Double-> [CGState sh]
cg op rhs initial =
    runIdentity $ do
      !rInit <- computeP $ rhs -^ op initial
      !r2Init <- normSquaredP rInit
      return $ iterate cgStep (CGState initial rInit rInit r2Init)
        where normSquaredP = sumAllP . R.map (**2)
              scale a = R.map (* a)
              cgStep (CGState !x !p !r !r2) =
                  -- We need a monad to avoid nested parallelism; the Identity
                  -- works fine.
                  runIdentity $ do
                    -- Here, the type checker could not deduce the type of `q`
                    -- if we used `computeP`, since it is polymorphic in its
                    -- output representation and (*^) below is polymorphic in
                    -- its input representation. `computeUnboxedP` is the same
                    -- as `computeP`, but has a more specific type.
                    !q <- unboxP $ op p
                    !qp <- sumAllP $ q *^ p
                    let alpha = r2 / qp
                    !x' <- computeP $ x +^ scale alpha p
                    !r' <- computeP $ r -^ scale alpha q
                    !r2' <- normSquaredP r'
                    let beta = r2' / r2
                    !p' <- computeP $ r' +^ scale beta p
                    return $ CGState x' p' r' r2'

-- `cgreg` is a variation of CG that solves
--
--   (T^* T + a I) x = T^* y
--
-- where `T` is an arbitraty linear mapping, `T^*` is the adjoint, `a` is a
-- positive number (the regularization parameter), and `I` is the identity map.

cgreg :: (Shape sh, Unboxable r sh Double, Source r Double) => (Array U sh Double -> Array r sh Double) -> (Array U sh Double -> Array r sh Double) -> Double -> Array U sh Double -> Array U sh Double -> [CGState sh]
cgreg op adjoint reg rhs initial =
    runIdentity $ do
      res <- computeUnboxedP $ rhs -^ op initial
      rInit <- unboxP $ adjoint res
      r2Init <- normSquaredP rInit
      return $ iterate cgStep (CGState initial rInit rInit r2Init)
        where normSquaredP = sumAllP . R.map (**2)
              scale a = R.map (* a)
              cgStep (CGState x p r r2) =
                  runIdentity $ do
                    !q <- unboxP $ op p
                    !p2 <- normSquaredP p
                    !q2 <- normSquaredP q
                    let alpha = r2 / (q2 + reg*p2)
                    !x' <- computeP $ x +^ scale alpha p
                    !s <- unboxP $ adjoint q
                    !r' <- computeP $ r -^ scale alpha (s +^ scale reg p)
                    !r2' <- normSquaredP r'
                    let beta = r2' / r2
                    !p' <- computeP $ r' +^ scale beta p
                    return $ CGState x' p' r' r2'

-- takeUntil is like takeWhile, but also returns the final iterate (no need to
-- waste it).

takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil _ [] = []
takeUntil predicate (x:xs)
    | predicate x = [x]
    | otherwise   = x : takeUntil predicate xs

-- We want to be able to output some information about the iterates, i.e. run an
-- IO action over the list of iterates, before returning the final one. One
-- option would be something like
--
--   fmap last . forM iterates $ \it -> do [...]
--
-- However, this would perform the `fmap last` only *after* the IO actions are
-- done, therfore retaining the entire list of iterates in memory.
--
-- We want all but the final iterate to be garbage collected directly after
-- printing information. This can be done using a monadic fold, where we do not
-- actually accumulate a result but only return the last value.

process :: Monad m => [a] -> (a -> m ()) -> m a
process xs f = foldM (\_ x -> f x >> return x) undefined xs

-- `runCG` and `runCGreg` call `cg` and `cgreg`, respectively, with initial
-- guess 0, use a stopping rule based on residuals relative to the first initial
-- guess, and output the residual while iterating -- therefore, they returns a
-- result in the IO monad.

runCG :: (Shape sh, Unboxable r sh Double, Source r Double) => Double -> (Array U sh Double -> Array r sh Double) -> Array U sh Double -> IO (Array U sh Double)
runCG tol op = runCG0 tol (cg op)

runCGreg :: (Shape sh, Unboxable r sh Double, Source r Double) => Double -> (Array U sh Double -> Array r sh Double) -> (Array U sh Double -> Array r sh Double) -> Double -> Array U sh Double -> IO (Array U sh Double)
runCGreg tol op adjoint reg = runCG0 tol (cgreg op adjoint reg)

runCG0 :: Shape sh => Double -> (Array U sh Double -> Array U sh Double -> [CGState sh]) -> Array U sh Double -> IO (Array U sh Double)
runCG0 tol go rhs = do
    let initial = computeS $ fromFunction (extent rhs) (const 0)
    let steps' = go rhs initial
    let r20 = cgr2 $ head steps'
    let steps = takeUntil (\x -> sqrt (cgr2 x / r20) < tol) steps'
    result <- process (zip [(1::Int)..] steps) $ \(n, cgs) ->
        putStrLn $ show n ++ "  " ++ show (sqrt $ cgr2 cgs / r20)
    return $ cgx . snd $ result

----------------------------
-- Saving and loading images
----------------------------

saveArrayAsPNG :: Source r Double => FilePath -> (Int, Int) -> Array r DIM2 Double -> IO ()
saveArrayAsPNG fname size array =
    P.writeFigure P.PNG fname size $ do
         P.setPlots 1 1
         P.withPlot (1, 1) . P.setDataset . repa2hm . copyS $ array

saveArrayAsBMP :: Source r Double => FilePath -> Array r DIM2 Double -> IO ()
saveArrayAsBMP file img = do
  !m <- foldAllP max 0 img
  !img' <- computeP $ R.map (rgb8OfGreyDouble . (/ m)) img
  writeImageToBMP file img'

loadArrayFromBMP :: (Source r Double, Target r Double) => FilePath -> IO (Array r DIM2 Double)
loadArrayFromBMP file = do
  loaded <- readImageFromBMP file
  case loaded of
    Left err -> error $ show err
    Right img -> computeP $ R.map doubleLuminanceOfRGB8 img
