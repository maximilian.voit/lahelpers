{ nixpkgs ? import <nixpkgs> {} }:
# { nixpkgs ? import https://nixos.org/channels/nixos-unstable {} }:

let

  haskellPackages = nixpkgs.haskellProfilingPackages;
  # haskellPackages = nixpkgs.haskellPackages;

in nixpkgs.ihaskell.override (args: {
  inherit (haskellPackages) ihaskell ghcWithPackages;
  packages = pkgs: (args.packages pkgs) ++ (with pkgs; [
    ghc-mod
    alex
    cabal-install
    cassava
    cpphs
    diagrams
    either
    gtk2hs-buildtools
    happy
    hdevtools
    hlint
    hmatrix
    hmatrix-gsl
    htrace
    ihaskell-charts
    ihaskell-diagrams
    ihaskell-display
    ihaskell-juicypixels
    ihaskell-plot
    repa
    repa-devil
    repa-io
    repa-fftw
    repa-algorithms
    repa-linear-algebra
    # lahelpers
    monad-par
    monad-state
    normaldistribution
  ]);
})
