{ nixpkgs ? import <nixpkgs> {} }:

let

  pkg = import ./. { inherit nixpkgs; };

in nixpkgs.runCommand (baseNameOf ./.) {
  buildInputs = [ pkg ];
  shellHook = ''
    export NIX_GHC=$(type -P ghc)
    export NIX_GHC_PKG=$(type -P ghc-pkg)
    export NIX_GHC_DOCDIR="$(realpath -ms $NIX_GHC/../../share/doc/ghc/html)"
    export NIX_GHC_LIBDIR="$(realpath -ms $NIX_GHC/../../lib/ghc-$($NIX_GHC --numeric-version))"
  '';
} ""
